---
weight: 50
title: Fundamentos
---

# Fundamentos

Imprimir un texto al terminal es la primera cosa que se hace con muchos lenguajes de programación. Pero, ¿qué haces si quieres entrar algo al terminal? Para esto existe la función `input` que te permite introducir texto al programa.

> Pon este programa pequeño en un archivo con <code>.py</code> al fin de su nombre.

```python
nombre = input("Hola, ¿cómo te llamas?")
print(f"¡Hola, {nombre}!")
```

El programa a la derecha es una modificación muy pequeña de tu primer programa. ¿Ves la diferencia? Ya no dice solamente «¡Hola!», pero después de introducir tu nombre y presionar la tecla enter, dice por ejemplo «¡Hola, Nico!». Vamos a analizar lo que pasa aquí.

En la primera línea del código encuentras tu primer uso del operador `=` que introduce una referencia que se llama `nombre` al programa. Puedes encontrar más informaciones sobre qué es el operador `=` y cómo lo funciona con las referencias en la sección «[Nombrando datos](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html#nombrando-datos)» del capítulo «[Trabajando con datos](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html#trabajando-con-datos)». Lo importante es saber que el texto que introduces al terminal lo puedes acceder en tu programa por la referencia `nombre`. Lo que te da la funciona `input` lo pones en una variable con el operador `=`.

La segunda línea del código también lleva algo nuevo para ti. En vez de imprimir «¡Hola!» la funciona `print` ahora usa la referencia `nombre` para imprimir por ejemplo «¡Hola, Nico!». Para poder usar la referencia en el String, tienes que poner una «f» (de «formateado») ante las comillas. Puedes encontrar [una explicación del tipo String](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html#string) en la sección «[Tipos](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html#tipos)». La «f» ante las comillas te permite poner código entre las llaves {...}.

<aside class="success">
Qué bueno tener el primer programa interactivo, ¿verdad?
</aside>

## El condicional if

Aumentamos un poquito la complejidad del programa. Con la ayuda del condicional `if` (ver la sección «[Condicionales](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html#condicionales)») puedes controlar lo que pasa en tu programa dependiendo de sus condiciones.

```python
nombre1 = input("Hola, ¿cómo te llamas?")
edad1_input = input("¿Cuántos años tienes?")

nombre2 = input("Hola, ¿cómo te llamas tú?")
edad2_input = input("¿Cuántos años tienes?")

edad1 = int(edad1_input)
edad2 = int(edad2_input)

if edad1 > edad2:
	print(f"{nombre1} tiene más años que {nombre2}")
else:
	print(f"{nombre2} tiene más años que {nombre1}")
```

Antes de seguir leyendo estas explicaciones, intenta entender el código a tu misma.

Como en el programa anterior, usamos referencias, las funcionas `input` y `print` y el operador `=`. Lo nuevo ahora es la función `int` y el condicional `if` con su compañero `else`. Para poder usar el condicional tenemos que dar una [prueba de verdad](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html#pruebas-de-verdad) (de la sección «[Operadores](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html#operadores)»). En nuestro caso queremos comparar las edades de las dos personas. Lo podemos hacer con facilidad con `edad1 > edad2`.

Pero, ¿qué pasa con `edad1_input` y `edad2_input`? ¿Por qué no podemos usar estas referencias directamente en el `if`?

La respuesta a estas preguntas es que la función `input` siempre te da un String. Sin embargo las edades son números enteros que quieres comparar. Para números enteros existe el tipo [Integer](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html#integer). Con la función `int` puedes convertir el String a un Integer (ver la sección «[Conversión](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html#conversi%C3%B3n)» para más detalles).

### Ejercicio

```
Hola, ¿cómo te llamas? Sofía
¿Cuántos años tienes? 24
¿Cuántos hermanos tienes? 3
Hola, ¿cómo te llamas? Juan
¿Cuántos años tienes? 26
¿Cuántos hermanos tienes? 2
Juan tiene más años que Sofía, pero Sofía tiene más hermanos que Juan.
```

Es hora de tu primer programa propio. Basado en el programa anterior, te pedimos que programes algo en python que no solo pregunta cuántos años tienen las dos personas, pero también cuántos hermanos tienen. A la derecha puedes ver un ejemplo del terminal.

Hay muchas opciones diferentes de hacer este programa. Por ejemplo, puedes hacer un `if [...] else` dentro de un `if`, o puedes hacer un `if` para cada posibilidad. Tú eliges cómo lo quieres hacer.

## Repetición y listas

¿Te has dado cuenta que en el ejemplo anterior tenemos código duplicado? La lógica para una persona es exactamente la misma que para la otra. Podemos abstraer esta lógica con la ayuda de bucles.

```python
nombres = list()
for i in [0, 1, 2]:
	nombre = input("Introduce un nombre, por favor: ")
	nombres.append(nombre)
for i in [0, 1, 2]:
	print(f"¡Hola, {nombres[i]}!")
```

Siempre cuando tienes algo que se repita en tu código puedes usar los bucles _for_ o _while_ (ver la sección «[Bucles](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html#bucles)» en el libro de ElenQ). A la derecha puedes ver un ejemplo de código que recoge nombres en la lista `nombres`. ¿Qué piensas hace el código y cuántos nombres recoge?

Son tres veces que el programa te pide introducir nombres al terminal. Es por esta línea de código: `for i in [0, 1, 2]`. Esto significa que el código en el bloque debajo del `for` está ejecutado una vez con `i = 0`, otra vez con `i = 1` y al final con `i = 2`. Cada vez te pregunta por un nombre y después lo agrega (en inglés «`append`») a la lista de `nombres`.

En el segundo bucle el código hace nada más que imprimir los nombres al terminal. Otra vez tenemos valores de `i` con `1`, `2` y `3` y los usamos para recibir los nombres de la lista. El código `nombres[0]` te da el nombre en la posición `0` de la lista `nombre`.

### Ejercicio

```
Introduce un nombre, por favor: Sofía
¿Cuántos años tiene? 24
¿Cuántos hermanos tiene? 3
Introduce un nombre, por favor: Juan
¿Cuántos años tiene? 26
¿Cuántos hermanos tiene? 2
Introduce un nombre, por favor: Pablo
¿Cuántos años tiene? 25
¿Cuántos hermanos tiene? 1
```

Ahora te toca a ti. Por favor, programe algo que no solo recoge nombres de personas, sino también sus edades y cantidades de hermanos. Es muy similar al programa anterior, pero esta vez usarás bucles y listas para recoger una cantidad flexible de personas e informaciones. Puedes ver un ejemplo a la derecha con tres personas.

Si necesitas una indicación para el comienzo: empezaríamos con el código de arriba, pero antes del bucle de imprimir pones otro código que por ejemplo empiezo con crear una nueva lista `edades`.
