---
weight: 10
title: Introducción
---

# Introducción

¡Bienvenida a este curso de programación!

Después de haber participado en este curso, esperamos que puedas crear tus primeros programas propios.

El curso empieza de cero, así que no tienes que tener ningún conocimiento antes. Te damos unas indicaciones sobre cómo instalar el sistema alternativo «[Linux Mint](https://linuxmint.com/)» en tu computadora para después aprender a usar el lenguaje de programación «[Python](https://es.wikipedia.org/wiki/Python)».

Cuando inició este proyecto, lo vimos como un todo integral. No solo te contara sobre la programación de Python, pero también te diera ejercicios para que pudieras practicarlo. Unos meses después del comienzo, decidimos cambiar el enfoque un poquito, porque ya existían cursos maravillosos. Por eso, este curso de ¡A Programar! se enfoque en darte ejercicios con los que puedes poner en práctica lo que puedes aprender por estos dos cursos siguientes.

## Programar en Python por Ekaitz Zárraga

[![Libro «Programar en Python»](/aprogramar/images/programar-python-elenq.png)](/aprogramar/images/programar-python-elenq.png)

De la página web de [Programar en Python](https://publishing.elenq.tech/es/#programming-in-python):

«Escrito por [Ekaitz Zárraga](https://ekaitz.elenq.tech/pages/about.html) como material para sus cursos de formación, este documento es un manual del lenguaje de programación Python que describe además fundamentos técnicos de la programación general.

\[...\]

Todos \[los libros] han sido publicados bajo los términos de la licencia [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.es) y se han preparado para su publicación en varios formatos: se publican en papel, en formato web y en un archivo de fácil impresión con el fin de que puedan llegar a cualquiera.»

Puedes acceder los libros como [PDF imprimible](https://publishing.elenq.tech/books/Programming_in_Python/es/book-simple.pdf) y [página web](https://publishing.elenq.tech/books/Programming_in_Python/es/web.html) (también hay una [versión simple](https://publishing.elenq.tech/books/Programming_in_Python/es/web-simple.html)).

En ¡A Programar! vamos a trabajar por la mayoría del tiempo con este libro. Sin embargo, también te queremos ofrecer una alternativa:

## Inmersión en Python 3 por Mark Pilgrim

[![Libro «Inmersión en Python 3»](/aprogramar/images/inmersion-en-python3.png)](/aprogramar/images/inmersion-en-python3.png)

Escrito por [Mark Pilgrim](https://en.wikipedia.org/wiki/Mark_Pilgrim) y traducido al español por [José Miguel González Aguilera](http://www.jmgaguilera.com/), me gusta este libro por la filosofía descrito en [el primer capítulo «Tu primer programa en Python»](http://www.jmgaguilera.com/inmersionenpython3html/node3.html#SECTION00310000000000000000):

«Los libros sobre programación suelen comenzar con varios capítulos sobre los fundamentos y van, poco a poco, avanzando hasta llegar a hacer programas útiles. Vamos a saltarnos todo eso. Lo primero que vamos a ver es un programa Python completo.»

Puedes acceder los libros del [proyecto Git](https://github.com/jmgaguilera/inmersionenpython3#inmersi%C3%B3n-en-python-3) como [PDF](https://github.com/jmgaguilera/inmersionenpython3/releases/download/v1.0/inmersionEnPython.pdf) y [página web](http://www.jmgaguilera.com/inmersionenpython3html).
