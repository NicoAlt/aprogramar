---
weight: 30
title: Linux Mint 
---

# Linux Mint

Linux Mint es un sistema alternativo que te recomendamos para este curso. Está totalmente bien usar Windows con este curso, pero si empiezas de cero y quieres conseguir un trabajo como desarrolladora, puedes empezar a usar Linux Mint en tu computadora, porque se lo usa mucho el Linux en los servidores también.

<aside class="notice">
Linux Mint usa el «núcleo»/«kernel» Linux por contraste a Windows o macOS que usan otros núcleos.
</aside>

Hay un [guía de instalación de Linux Mint](https://linuxmint-installation-guide.readthedocs.io/es/latest/) extensiva que tiene todas las informaciones que necesitas. Aquí puedes [descargar el sistema](https://linuxmint.com/download.php). Si no tienes ninguna idea, es la versión «Cinnamon» que quieres descargar. Después de descargar el «imagen de instalación» de formato _.iso_, lo pones en una memoria USB con herramientas como [Etcher](https://etcher.io/) o [Rufus](https://rufus.ie/). Después de arrancar tu computadora de la memoria, puedes seguir las instrucciones de Linux Mint para instalarlo en tu computadora.

<aside class="warning">
Hay que tener cuidado si quieres mantener tu sistema anterior en tu computadora. Por defecto Linux Mint borra todo de tu disco duro.
</aside>

Si lograste instalar Linux Mint: ¡felicidades! Es el primer paso importante en la dirección a aprender a programar.

## Instalación de Geany

Lo bueno de Linux Mint (y muchos sistemas Linux en general) es que ya lleva todo para empezar a programar python. La única cosa que te recomendamos es instalar el editor «Geany», porque el editor estándar de Linux Mint no tiene muchas funcionalidades. En Linux Mint puedes instalar software de una manera muy fácil con el «Gestor de software». Una alternativa que siempre puedes usar es el terminal. Con el terminal puedes hacer todo lo que te puedes imaginar. Para instalar Geany, abres el terminal y entras: `sudo apt install geany`

Después de entrarlo al terminal, tienes que presionar la tecla enter. Te va a preguntar el terminal si es geany lo que quieres instalar. Lo puedes aprobar con otra tecla enter.

Vamos a usar el terminal más adelante cuando vamos a correr nuestro primer programa de python.
