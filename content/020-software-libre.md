---
weight: 20
title: Software Libre
---

# Software Libre

Una cosa muy linda es el software libre. A diferencia del software propietario el software libre [te ofrece muchas libertades](https://es.wikipedia.org/wiki/Software_libre#Las_cuatro_libertades_del_software_libre). Estás libre de usar el software sin tener que pedir permiso de ningún empresa o organización. Tienes la libertad de leer y estudiar el código del software libre y también puedes modificar el software para que te cabe mejor. Si quieres, tienes el derecho de distribuir copias del software. Por último tienes la libertad de mejorar el software y distribuir las versiones mejoradas. El autor de estas líneas básicamente aprendió todo de lo siguiente por participar en comunidades del software libre. Estas comunidades del software libre tienen miembros de todo el mundo y juntos crean los programas accesibles para todas y todos. Del sistema Linux Mint por el editor Geany al lenguaje de programación Python, cada software usado en este curso es software libre.

[![Amo al software libre](/aprogramar/images/ilovefs.png)](https://ilovefs.org)
