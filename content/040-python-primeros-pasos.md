---
weight: 40
title: Python 
---

# Python

Python es un lenguaje de programación muy popular en el mundo. Es conocido por ser fácil de aprender y por tener bastante funcionalidad disponible en ambos la «librería» estándar como en las librerías de la comunidad. Cuando aprendes a programar python, no es que siempre tienes que seguir con python. Cuando sabes programar en un lenguaje de programación es muy fácil aprender otro lenguaje.

> Pon este comando en un archivo con <code>.py</code> al fin de su nombre.

```python
print("¡Hola!")
```

El código que ves a la derecha de este texto va a ser tu primer propio programa (si lo lees en un celular, el código está por encima de aquí). Es un programa muy fácil que hace una cosa: imprimir el texto «¡Hola!».

Como dice por encima del código, tienes que poner esta línea de código en un archivo de texto. Lo puedes crear con el editor Geany. En Geany solo entras el código y lo guardas todo en un archivo como por ejemplo _hola.py_. Lo importante es que lleva el _.py_ al fin para que puedes reconocer que es un programa python.

Ahora tienes dos opciones: puedes correr el programa directamente en Geany o lo corres en el terminal.

Para correrlo en Geany, solo tienes que presionar la tecla F5. Si te sale una error, tal vez tienes que [modificar la configuración de Geany](https://stackoverflow.com/questions/29105941/how-do-i-make-python3-the-default-python-in-geany/34317310#34317310) para poder usar el F5.

También te vamos a mostrar cómo correr el programa en el terminal. En el futuro vamos a dejar solo las informaciones para el terminal, porque es más universal. Para empezar tienes que abrir el administrador de archivos. Navegas a donde guardaste el archivo y allí das un clic del ratón de la derecha en el area blanca. En el menú puedes dar clic en «Abrir en el terminal».

Cuando lo tienes abierto, es muy fácil correr el programa lo que acabas de guardar. Todo lo que tienes que hacer es entrar `python3 hello.py` (o el nombre de tu archivo) y tocar la tecla enter. Te debería salir el «¡Hola!» debajo de la primera línea.

<aside class="success">
¡Felicidades! Has programado tu primer propio programa.
</aside>
