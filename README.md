# ¡A Programar! - dorfbrunnen.eu

This is the source code of the website
[¡A Programar!](https://nicoalt.codeberg.page/aprogramar/).
It's a Spanish-speaking website teaching people how to program with Python, starting from zero.

## Building

You need to have [Hugo](https://gohugo.io) >= 0.62.2 installed.

```
sudo pacman -S hugo
```

To build the website, simply run `hugo`.

If you want to build the website and
serve it with a local server at [localhost:1313](http://localhost:1313),
use:

```
hugo server
```

## License

The source code and content of this website is licensed under the
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/),
if not stated differently.
